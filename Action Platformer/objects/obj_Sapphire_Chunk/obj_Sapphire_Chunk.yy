{
    "id": "852f1dd3-01a6-4097-be29-c75d2ea4332a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Sapphire_Chunk",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c48c2d65-f035-4367-a339-b123a2872e82",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fe620979-41ce-4f73-bedc-c0cee6a6f308",
    "visible": true
}