/// Draw the dirt

draw_self();

var right = place_meeting(x + 1 , y, obj_Solid);
var left = place_meeting(x - 1 , y, obj_Solid);
var up = place_meeting(x, y - 1, obj_Solid);
var down = place_meeting(x, y + 1, obj_Solid);

if (!up) {
	draw_sprite_ext(spr_dirt_top, 0, x, y - 4, 1, 1, 0, c_white, 1);
}

if (!down) {
	draw_sprite_ext(spr_dirt_edge, 0, x + 16, y + 16, 1, 1, 270, c_white, 1);
}

if (!right) {
	draw_sprite_ext(spr_dirt_edge, 0, x + 16, y + 16, 1, 1, 0, c_white, 1);
}

if (!left) {
	draw_sprite_ext(spr_dirt_edge, 0, x + 16 , y + 16, 1, 1, 180, c_white, 1);
}