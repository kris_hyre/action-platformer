{
    "id": "e0631230-fc78-44f3-97c8-651e63793db6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Exit",
    "eventList": [
        {
            "id": "9ccd03bf-29fa-4ddb-b482-1dc18b026454",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e0631230-fc78-44f3-97c8-651e63793db6"
        },
        {
            "id": "1880501c-2a6a-42c9-af8d-0d34e33b258a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e0631230-fc78-44f3-97c8-651e63793db6"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c9808762-3485-4ba6-b419-53e4d20bf539",
    "visible": true
}