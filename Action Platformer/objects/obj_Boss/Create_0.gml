/// @description Initialize the boss

hspd = 0;
vspd = 0;
image_speed = 0;
state = boss_idle_state;
hp = 3;
depth = 85;

// Boss music
audio_em = audio_emitter_create();

if (audio_is_playing(mus_Malicious)){
		audio_stop_sound(mus_Malicious);
}

