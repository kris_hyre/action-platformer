/// @description Execute state

script_execute(state);

// Draw red when damaged by lava
if (place_meeting(x, y, obj_Lava)){
	image_blend = c_red;	
} else {
	image_blend = c_white;	
}

// Death
if (hp <= 0){
	repeat(50){
		instance_create_layer(bbox_left + random(sprite_width - 24), bbox_top + random(sprite_height), "Instances", obj_Fire_Bubble);
	}
	
	if (audio_is_playing(mus_Clenched_Teeth)){
		audio_stop_sound(mus_Clenched_Teeth);	
	}
	
	instance_destroy();
}