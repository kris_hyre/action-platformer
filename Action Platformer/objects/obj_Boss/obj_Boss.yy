{
    "id": "73b045ae-fae5-4f98-a87f-d29b6d450881",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Boss",
    "eventList": [
        {
            "id": "5c3f15f3-3443-47c1-af47-8ee1e5650884",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "73b045ae-fae5-4f98-a87f-d29b6d450881"
        },
        {
            "id": "2d51331b-0951-4915-a4eb-c276aea35d85",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "73b045ae-fae5-4f98-a87f-d29b6d450881"
        },
        {
            "id": "e63ad852-8553-4205-9c6f-92e6db06095d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "73b045ae-fae5-4f98-a87f-d29b6d450881"
        },
        {
            "id": "501e619c-5ca1-4dca-96b3-47e08c2a384b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4f64c114-bcc6-4420-9fae-2c0f11c87f91",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "73b045ae-fae5-4f98-a87f-d29b6d450881"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "6f7f8448-6f91-4549-aa0a-10784a0cd821",
    "visible": true
}