{
    "id": "e0f20feb-5240-45e6-baac-0fbcc3e04f6e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Sapphire",
    "eventList": [
        {
            "id": "9df993c7-335b-420b-b27c-4582450b3c56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "e0f20feb-5240-45e6-baac-0fbcc3e04f6e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d6b705ce-abbe-48a8-b115-59d1ee0b35e0",
    "visible": true
}