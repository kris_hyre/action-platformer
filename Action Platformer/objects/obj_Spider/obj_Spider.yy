{
    "id": "bb4fec15-0031-4226-a77b-d9816f447674",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Spider",
    "eventList": [
        {
            "id": "da5ea8ea-2f43-4d0e-9b99-6c6d7c6633df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bb4fec15-0031-4226-a77b-d9816f447674"
        },
        {
            "id": "50eb1b39-56e2-4628-8aac-64a2240827eb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bb4fec15-0031-4226-a77b-d9816f447674"
        },
        {
            "id": "f58d69d4-4806-410c-8fb2-7ba2bfce2240",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "bb4fec15-0031-4226-a77b-d9816f447674"
        },
        {
            "id": "2dee8ab6-e8bc-423e-9aa3-0282c69b31b2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 7,
            "m_owner": "bb4fec15-0031-4226-a77b-d9816f447674"
        },
        {
            "id": "6d730ff9-96b1-4ed1-8f44-89c29cc7ec92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "bb4fec15-0031-4226-a77b-d9816f447674"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2bcbb83b-f903-4e62-99ab-e525c692cf0b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "873f09f6-063f-42ee-9b13-90d7ea79e32a",
    "visible": true
}