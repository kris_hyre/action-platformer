{
    "id": "94b7b9ae-9328-4458-88c7-0aa20dc50b14",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Lava",
    "eventList": [
        {
            "id": "70609ad4-b56c-4012-840a-91b6c7f5afc1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "94b7b9ae-9328-4458-88c7-0aa20dc50b14"
        },
        {
            "id": "507ce656-53c9-4bd9-9030-1297fbf6f905",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "94b7b9ae-9328-4458-88c7-0aa20dc50b14"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c3e58e93-ad51-4c7f-a409-c906318e0b9e",
    "visible": true
}