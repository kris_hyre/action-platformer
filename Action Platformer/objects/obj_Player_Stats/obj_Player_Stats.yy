{
    "id": "36ac5fca-7109-4be8-b97c-40b591d23137",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Player_Stats",
    "eventList": [
        {
            "id": "9128ab8b-6d96-4a4d-88a4-5d78c0673fa2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "36ac5fca-7109-4be8-b97c-40b591d23137"
        },
        {
            "id": "0800dea1-c96e-400b-83ef-fd682162a843",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "36ac5fca-7109-4be8-b97c-40b591d23137"
        },
        {
            "id": "e16a99e0-04ef-47f2-9393-712e88e4554a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "36ac5fca-7109-4be8-b97c-40b591d23137"
        },
        {
            "id": "9cdf67d2-4cf1-4b46-95dc-03a9baed5044",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "36ac5fca-7109-4be8-b97c-40b591d23137"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}