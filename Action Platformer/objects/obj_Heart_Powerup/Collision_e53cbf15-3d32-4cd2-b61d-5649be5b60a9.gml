/// @description Heal Player

if (obj_Player_Stats.hp < obj_Player_Stats.max_hp){
	obj_Player_Stats.hp += 1;
}

instance_destroy(self);