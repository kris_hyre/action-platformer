{
    "id": "36eeefb6-d95c-467c-9294-7a4b7ef724db",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_Heart_Powerup",
    "eventList": [
        {
            "id": "e53cbf15-3d32-4cd2-b61d-5649be5b60a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "cbe2ca78-686a-4273-887f-52a45b6717c8",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "36eeefb6-d95c-467c-9294-7a4b7ef724db"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "006d2455-affb-41df-91fd-d26ef5b77efb",
    "visible": true
}