/// Inital Values

spd = 6;
hspd = 0;
vspd = 0;
grav = 1.5;
acc = 2
jspd = 16;
depth = 90;

state = move_state;

// Get player input
get_input();

// Create audio emitter for player sounds
audio_em = audio_emitter_create();