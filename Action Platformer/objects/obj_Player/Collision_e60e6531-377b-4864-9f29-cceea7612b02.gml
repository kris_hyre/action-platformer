/// Collect sapphires
obj_Player_Stats.sapphires += 1;
with (other){
	instance_destroy();
	audio_play_sound(snd_tink, 4, false);
}