{
    "id": "006d2455-affb-41df-91fd-d26ef5b77efb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_heart",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0bbf1a33-f3ca-4087-a990-eb6bc924aa05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "006d2455-affb-41df-91fd-d26ef5b77efb",
            "compositeImage": {
                "id": "269e185b-e4ac-45cf-b2ca-57aaf5aebba2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bbf1a33-f3ca-4087-a990-eb6bc924aa05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c6cf4de-b218-4a13-a96e-6130fe780fc1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bbf1a33-f3ca-4087-a990-eb6bc924aa05",
                    "LayerId": "2bdb2887-2144-483d-a1f4-b374d6b6f6a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2bdb2887-2144-483d-a1f4-b374d6b6f6a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "006d2455-affb-41df-91fd-d26ef5b77efb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}