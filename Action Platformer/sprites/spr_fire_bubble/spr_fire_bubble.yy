{
    "id": "a05dbb27-17c8-478f-b481-fdec851340a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_fire_bubble",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "df914fa7-53be-40cd-af3b-478f216d3c0e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05dbb27-17c8-478f-b481-fdec851340a7",
            "compositeImage": {
                "id": "4fd9a336-38ef-4ab3-b165-f73958489395",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df914fa7-53be-40cd-af3b-478f216d3c0e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0512de8b-277a-44a1-b973-390bed64aa7a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df914fa7-53be-40cd-af3b-478f216d3c0e",
                    "LayerId": "9a79daa6-7f9a-4b78-b1f4-9e207a96d6e6"
                }
            ]
        },
        {
            "id": "8f8ec67e-2a98-40e6-8282-86063f61bd7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05dbb27-17c8-478f-b481-fdec851340a7",
            "compositeImage": {
                "id": "377193aa-4710-4c20-b690-46a6496cb857",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8f8ec67e-2a98-40e6-8282-86063f61bd7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c378d772-c06d-4429-81c1-098d4c21ee59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8f8ec67e-2a98-40e6-8282-86063f61bd7e",
                    "LayerId": "9a79daa6-7f9a-4b78-b1f4-9e207a96d6e6"
                }
            ]
        },
        {
            "id": "b5e9d795-7661-4099-9762-b51f912eb181",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05dbb27-17c8-478f-b481-fdec851340a7",
            "compositeImage": {
                "id": "ac8b4b4e-7f68-415c-a376-97e8c9b90268",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5e9d795-7661-4099-9762-b51f912eb181",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b957eb8c-30bb-4fd7-b6f7-a5da8aef733e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5e9d795-7661-4099-9762-b51f912eb181",
                    "LayerId": "9a79daa6-7f9a-4b78-b1f4-9e207a96d6e6"
                }
            ]
        },
        {
            "id": "ae70c27c-9f40-4293-a5b4-942884edc39f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a05dbb27-17c8-478f-b481-fdec851340a7",
            "compositeImage": {
                "id": "50d1fd59-d67e-4318-b2a9-76e24206a8f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ae70c27c-9f40-4293-a5b4-942884edc39f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3c244c3-ecb2-48e2-b94b-f836d49bb24c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ae70c27c-9f40-4293-a5b4-942884edc39f",
                    "LayerId": "9a79daa6-7f9a-4b78-b1f4-9e207a96d6e6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "9a79daa6-7f9a-4b78-b1f4-9e207a96d6e6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a05dbb27-17c8-478f-b481-fdec851340a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}