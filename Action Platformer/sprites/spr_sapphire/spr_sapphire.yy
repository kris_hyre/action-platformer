{
    "id": "d6b705ce-abbe-48a8-b115-59d1ee0b35e0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sapphire",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 19,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "007359be-95c4-4337-9a30-8feb9a2a15a3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d6b705ce-abbe-48a8-b115-59d1ee0b35e0",
            "compositeImage": {
                "id": "561fbe53-b421-49fd-b5c6-f73ceffea028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "007359be-95c4-4337-9a30-8feb9a2a15a3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44c1a3ce-c9ac-4092-b4eb-a44867aabc54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "007359be-95c4-4337-9a30-8feb9a2a15a3",
                    "LayerId": "93c2e3e4-ea12-4864-833f-abf9fa27d838"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 20,
    "layers": [
        {
            "id": "93c2e3e4-ea12-4864-833f-abf9fa27d838",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d6b705ce-abbe-48a8-b115-59d1ee0b35e0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 10
}