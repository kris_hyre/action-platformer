{
    "id": "af3ab69f-ba40-4d9c-9621-5f48129c7dea",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_ledge_grab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 2,
    "bbox_right": 28,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e346179c-c20b-4eb5-84ce-fb64f9232b3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "af3ab69f-ba40-4d9c-9621-5f48129c7dea",
            "compositeImage": {
                "id": "0efd1f11-a986-4eca-9b58-2f299c421090",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e346179c-c20b-4eb5-84ce-fb64f9232b3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c999679d-2147-4470-8250-70f231b3d021",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e346179c-c20b-4eb5-84ce-fb64f9232b3d",
                    "LayerId": "79483527-da7a-48ec-99df-f944621c4688"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "79483527-da7a-48ec-99df-f944621c4688",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "af3ab69f-ba40-4d9c-9621-5f48129c7dea",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 18
}