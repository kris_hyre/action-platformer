{
    "id": "474bd0fb-7790-4379-9041-84336500b8d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_snake",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c299b289-ad79-4f29-8a1e-c7768d2ad4fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "474bd0fb-7790-4379-9041-84336500b8d4",
            "compositeImage": {
                "id": "d7ca0260-8ea8-4596-b797-d46756457653",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c299b289-ad79-4f29-8a1e-c7768d2ad4fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "61d885c7-4fb2-41d3-b218-5675ce4fd695",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c299b289-ad79-4f29-8a1e-c7768d2ad4fe",
                    "LayerId": "3afc3f76-9696-4288-9bc7-90aa2ea2417c"
                }
            ]
        },
        {
            "id": "fbbf8f81-7504-4293-9b56-8e0dda1e579d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "474bd0fb-7790-4379-9041-84336500b8d4",
            "compositeImage": {
                "id": "fcdfd37b-129d-40f8-88cd-876888b9e439",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fbbf8f81-7504-4293-9b56-8e0dda1e579d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1779465-a0c9-42cf-b43e-8e5af95b9cb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fbbf8f81-7504-4293-9b56-8e0dda1e579d",
                    "LayerId": "3afc3f76-9696-4288-9bc7-90aa2ea2417c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3afc3f76-9696-4288-9bc7-90aa2ea2417c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "474bd0fb-7790-4379-9041-84336500b8d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}