{
    "id": "4d5e029b-0cfb-41c1-a851-ccf4e978c67b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 2,
    "bbox_right": 28,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7e26a81-af84-41c7-a4db-e2ecd1715ede",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d5e029b-0cfb-41c1-a851-ccf4e978c67b",
            "compositeImage": {
                "id": "198be34c-3470-4e76-813b-b20690058ce0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7e26a81-af84-41c7-a4db-e2ecd1715ede",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb99a1cf-f97b-416e-808e-acf58e7644ae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7e26a81-af84-41c7-a4db-e2ecd1715ede",
                    "LayerId": "b75080d9-4d5b-4a5d-9594-e0ea58b1d2da"
                }
            ]
        },
        {
            "id": "b08e3eb2-2e1b-4936-a607-38697abfbdc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d5e029b-0cfb-41c1-a851-ccf4e978c67b",
            "compositeImage": {
                "id": "2bc3f749-5be5-4a71-bf26-46d0731badb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b08e3eb2-2e1b-4936-a607-38697abfbdc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acf47fef-e376-4ebb-9a1f-d96310427e9a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b08e3eb2-2e1b-4936-a607-38697abfbdc8",
                    "LayerId": "b75080d9-4d5b-4a5d-9594-e0ea58b1d2da"
                }
            ]
        },
        {
            "id": "d7822f97-0e52-4dc4-920e-cd354ee99ec9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d5e029b-0cfb-41c1-a851-ccf4e978c67b",
            "compositeImage": {
                "id": "4dde37f7-f2ee-4d3c-bd99-2f84b8425300",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7822f97-0e52-4dc4-920e-cd354ee99ec9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "288a5031-59bb-47cc-8859-2ede630ed065",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7822f97-0e52-4dc4-920e-cd354ee99ec9",
                    "LayerId": "b75080d9-4d5b-4a5d-9594-e0ea58b1d2da"
                }
            ]
        },
        {
            "id": "6813ee03-d9d1-488e-a18e-29793c815f29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d5e029b-0cfb-41c1-a851-ccf4e978c67b",
            "compositeImage": {
                "id": "d1361bc4-c14a-4160-97ba-e2cdef0922dd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6813ee03-d9d1-488e-a18e-29793c815f29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "69aa9505-7715-42c2-8ce1-cd887fdd762d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6813ee03-d9d1-488e-a18e-29793c815f29",
                    "LayerId": "b75080d9-4d5b-4a5d-9594-e0ea58b1d2da"
                }
            ]
        },
        {
            "id": "9c578e50-1c3d-493c-8647-76d080aa35f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d5e029b-0cfb-41c1-a851-ccf4e978c67b",
            "compositeImage": {
                "id": "3e5e24ea-80b3-4241-bd15-4ae877a961b0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c578e50-1c3d-493c-8647-76d080aa35f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "295df8c8-39b2-469e-845e-9b9722e1ed6c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c578e50-1c3d-493c-8647-76d080aa35f4",
                    "LayerId": "b75080d9-4d5b-4a5d-9594-e0ea58b1d2da"
                }
            ]
        },
        {
            "id": "6d6165e8-836e-41cf-a7fd-cec0ae106c6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d5e029b-0cfb-41c1-a851-ccf4e978c67b",
            "compositeImage": {
                "id": "7155c158-74fe-48c9-bf84-3dda89e2cbe2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d6165e8-836e-41cf-a7fd-cec0ae106c6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cc210d7-a67c-4e8a-a956-ddf3f78efcb0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d6165e8-836e-41cf-a7fd-cec0ae106c6e",
                    "LayerId": "b75080d9-4d5b-4a5d-9594-e0ea58b1d2da"
                }
            ]
        },
        {
            "id": "b68167ca-d08f-40d8-aa2b-706745384849",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d5e029b-0cfb-41c1-a851-ccf4e978c67b",
            "compositeImage": {
                "id": "36ef0c43-4551-495e-9961-14d61679d10e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b68167ca-d08f-40d8-aa2b-706745384849",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5616c69f-d21a-4290-b7aa-4bde378b1e54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b68167ca-d08f-40d8-aa2b-706745384849",
                    "LayerId": "b75080d9-4d5b-4a5d-9594-e0ea58b1d2da"
                }
            ]
        },
        {
            "id": "52e40b05-1200-4fc8-b388-bdba78e6a797",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d5e029b-0cfb-41c1-a851-ccf4e978c67b",
            "compositeImage": {
                "id": "91ee79a8-0a85-4ae8-ba4d-8d85f2f0c8c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52e40b05-1200-4fc8-b388-bdba78e6a797",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7acf0c40-0527-4626-a40e-242403d0f145",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52e40b05-1200-4fc8-b388-bdba78e6a797",
                    "LayerId": "b75080d9-4d5b-4a5d-9594-e0ea58b1d2da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "b75080d9-4d5b-4a5d-9594-e0ea58b1d2da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d5e029b-0cfb-41c1-a851-ccf4e978c67b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 18
}