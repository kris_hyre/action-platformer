{
    "id": "fe620979-41ce-4f73-bedc-c0cee6a6f308",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sapphire_chunk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7832b3ef-93d7-45a0-a32c-811649fc5cbe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe620979-41ce-4f73-bedc-c0cee6a6f308",
            "compositeImage": {
                "id": "fea3dc69-ec00-49ed-aa61-4d93b1632db9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7832b3ef-93d7-45a0-a32c-811649fc5cbe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8fa2e3f-8676-4283-b3ff-742ca0d99bdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7832b3ef-93d7-45a0-a32c-811649fc5cbe",
                    "LayerId": "2aeddb30-9e20-47d3-9178-81da308ee966"
                }
            ]
        },
        {
            "id": "2f7a5f02-ff45-412a-9b5f-f515b4d04807",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe620979-41ce-4f73-bedc-c0cee6a6f308",
            "compositeImage": {
                "id": "e50ee93d-65b4-49ec-9884-a7d9e31534d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2f7a5f02-ff45-412a-9b5f-f515b4d04807",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c5594c72-762f-450c-bc5e-90241fa5c7e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2f7a5f02-ff45-412a-9b5f-f515b4d04807",
                    "LayerId": "2aeddb30-9e20-47d3-9178-81da308ee966"
                }
            ]
        },
        {
            "id": "35b983c7-0a71-4388-b58a-9832dc3c949e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe620979-41ce-4f73-bedc-c0cee6a6f308",
            "compositeImage": {
                "id": "5e1da1ed-07ab-46a7-abb7-7c572fac5e5f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35b983c7-0a71-4388-b58a-9832dc3c949e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d9e289da-fcc3-4e41-9834-12111ecb9950",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35b983c7-0a71-4388-b58a-9832dc3c949e",
                    "LayerId": "2aeddb30-9e20-47d3-9178-81da308ee966"
                }
            ]
        },
        {
            "id": "8205af0d-c758-45e2-b1c3-b363d6d8f976",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fe620979-41ce-4f73-bedc-c0cee6a6f308",
            "compositeImage": {
                "id": "f7f93d7b-4e69-4783-86e1-79fb6eb4e3ff",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8205af0d-c758-45e2-b1c3-b363d6d8f976",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c34f908-48b4-420d-aec3-18505a5cf778",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8205af0d-c758-45e2-b1c3-b363d6d8f976",
                    "LayerId": "2aeddb30-9e20-47d3-9178-81da308ee966"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "2aeddb30-9e20-47d3-9178-81da308ee966",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fe620979-41ce-4f73-bedc-c0cee6a6f308",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 4,
    "yorig": 4
}