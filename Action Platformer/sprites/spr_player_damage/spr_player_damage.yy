{
    "id": "0e1be8a8-d6f3-4ffc-8a15-55a394349d75",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_damage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 1,
    "bbox_right": 28,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "85180dff-ad2c-42ce-9230-a26d00eab6b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e1be8a8-d6f3-4ffc-8a15-55a394349d75",
            "compositeImage": {
                "id": "26fadb30-04e7-4ac4-8f8e-903d1592f56f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "85180dff-ad2c-42ce-9230-a26d00eab6b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "888b4011-7a3f-4aa3-8f7d-f3bc2c50ea0b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "85180dff-ad2c-42ce-9230-a26d00eab6b5",
                    "LayerId": "30d851a1-95da-4ba6-97e7-ced1a1b38356"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "30d851a1-95da-4ba6-97e7-ced1a1b38356",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e1be8a8-d6f3-4ffc-8a15-55a394349d75",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 18
}