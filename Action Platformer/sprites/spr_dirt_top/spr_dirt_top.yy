{
    "id": "640118fd-8f7d-4b73-b591-9a9e169a6299",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dirt_top",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2a2ec69-2cdc-4cf1-a43b-f4681ec50ad1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "640118fd-8f7d-4b73-b591-9a9e169a6299",
            "compositeImage": {
                "id": "ec78db7d-e1cb-4216-882a-fea4d16cd813",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2a2ec69-2cdc-4cf1-a43b-f4681ec50ad1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4c76697c-208d-4077-99d2-5b69cdd246d7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2a2ec69-2cdc-4cf1-a43b-f4681ec50ad1",
                    "LayerId": "8569006f-bb67-4740-96b6-3b43fdbbb32b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8569006f-bb67-4740-96b6-3b43fdbbb32b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "640118fd-8f7d-4b73-b591-9a9e169a6299",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}