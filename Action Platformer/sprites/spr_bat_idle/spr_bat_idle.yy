{
    "id": "7a6b0fd5-48e2-455b-9a37-6babfd14c443",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bat_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 5,
    "bbox_right": 25,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93a02c59-1a1e-4481-9cbe-3353475a9028",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a6b0fd5-48e2-455b-9a37-6babfd14c443",
            "compositeImage": {
                "id": "04dd890a-d0c9-4b0b-95f0-7227358075f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93a02c59-1a1e-4481-9cbe-3353475a9028",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13541d40-f47a-40bf-bd07-ffd80ab7dd91",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93a02c59-1a1e-4481-9cbe-3353475a9028",
                    "LayerId": "f3c4e8be-cb7e-4288-b236-1e6b637cbd2c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f3c4e8be-cb7e-4288-b236-1e6b637cbd2c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a6b0fd5-48e2-455b-9a37-6babfd14c443",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}