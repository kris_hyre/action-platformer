{
    "id": "7faef636-db3a-48c2-9613-1d05c01b8a55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_mask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 32,
    "bbox_left": 8,
    "bbox_right": 23,
    "bbox_top": 2,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "706fb06d-c6ae-4b8c-ae83-259db52d2930",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7faef636-db3a-48c2-9613-1d05c01b8a55",
            "compositeImage": {
                "id": "78502178-5954-40ac-ac81-da182c60d1b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "706fb06d-c6ae-4b8c-ae83-259db52d2930",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d0b591c-b2b9-40b6-ae05-c8b3a4ed6618",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "706fb06d-c6ae-4b8c-ae83-259db52d2930",
                    "LayerId": "4c0eee9c-6e36-4ef0-8584-29fd6403512b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "4c0eee9c-6e36-4ef0-8584-29fd6403512b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7faef636-db3a-48c2-9613-1d05c01b8a55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 14
}