{
    "id": "d3c72ba3-1303-471b-837b-fb0c54be92de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 2,
    "bbox_right": 28,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5304bb99-47d9-4f85-ae98-e102a6c62c42",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3c72ba3-1303-471b-837b-fb0c54be92de",
            "compositeImage": {
                "id": "5f1b2689-a396-471b-86e5-053ca014e23f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5304bb99-47d9-4f85-ae98-e102a6c62c42",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29fe0ed4-0f63-4a8e-b7a6-cdf44c7dc47d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5304bb99-47d9-4f85-ae98-e102a6c62c42",
                    "LayerId": "a06c5ca9-f1fd-434e-a50c-e6396ffb2ea2"
                }
            ]
        },
        {
            "id": "91660ef8-e775-48b0-a980-dc2153a6c045",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d3c72ba3-1303-471b-837b-fb0c54be92de",
            "compositeImage": {
                "id": "bd6f1d35-3263-436c-8c93-bf83def56d54",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "91660ef8-e775-48b0-a980-dc2153a6c045",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7b842306-6b13-40d6-8129-3df9ba397d2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "91660ef8-e775-48b0-a980-dc2153a6c045",
                    "LayerId": "a06c5ca9-f1fd-434e-a50c-e6396ffb2ea2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "a06c5ca9-f1fd-434e-a50c-e6396ffb2ea2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d3c72ba3-1303-471b-837b-fb0c54be92de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 18
}