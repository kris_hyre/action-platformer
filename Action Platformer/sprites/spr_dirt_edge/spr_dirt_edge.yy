{
    "id": "bfce83b4-3d41-41c3-927c-a493be81917d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dirt_edge",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 33,
    "bbox_left": 30,
    "bbox_right": 35,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c62b5b67-c90d-4d2c-ad61-e3b6a5685b2b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bfce83b4-3d41-41c3-927c-a493be81917d",
            "compositeImage": {
                "id": "61980f46-8848-47af-ba61-78a313dd5b68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c62b5b67-c90d-4d2c-ad61-e3b6a5685b2b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1ab7100-8f64-4485-88c3-e3399333b57d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c62b5b67-c90d-4d2c-ad61-e3b6a5685b2b",
                    "LayerId": "96165afa-2301-42da-8b45-713537fb8186"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 36,
    "layers": [
        {
            "id": "96165afa-2301-42da-8b45-713537fb8186",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bfce83b4-3d41-41c3-927c-a493be81917d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 36,
    "xorig": 18,
    "yorig": 18
}