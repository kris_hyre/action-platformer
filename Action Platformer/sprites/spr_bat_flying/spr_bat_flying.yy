{
    "id": "158ab91d-20e1-49ef-bdd9-d7ac5bdb9368",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bat_flying",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 2,
    "bbox_right": 29,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "26512b92-8740-48b2-8067-ec2c5362f569",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "158ab91d-20e1-49ef-bdd9-d7ac5bdb9368",
            "compositeImage": {
                "id": "6bdb33c9-ad28-4fc4-85b8-fd6abcfe1142",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "26512b92-8740-48b2-8067-ec2c5362f569",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5a8f864-64c3-4323-9a42-6c6ba25230cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "26512b92-8740-48b2-8067-ec2c5362f569",
                    "LayerId": "56559db9-3380-472d-8efc-f37f47802ec2"
                }
            ]
        },
        {
            "id": "f8ed57ba-607e-47d3-a648-22e70bed9998",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "158ab91d-20e1-49ef-bdd9-d7ac5bdb9368",
            "compositeImage": {
                "id": "dcb2833e-7107-425b-a067-d42ad8329502",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f8ed57ba-607e-47d3-a648-22e70bed9998",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7971437-ae12-4322-8894-8dbacc7bedd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f8ed57ba-607e-47d3-a648-22e70bed9998",
                    "LayerId": "56559db9-3380-472d-8efc-f37f47802ec2"
                }
            ]
        },
        {
            "id": "3a0f1a96-e2fa-4e0e-9afd-df52d4d91585",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "158ab91d-20e1-49ef-bdd9-d7ac5bdb9368",
            "compositeImage": {
                "id": "b845e0bd-e87f-461a-89f2-7a97e1f06758",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3a0f1a96-e2fa-4e0e-9afd-df52d4d91585",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f1e4a835-efaa-40d7-8568-c0410e537159",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3a0f1a96-e2fa-4e0e-9afd-df52d4d91585",
                    "LayerId": "56559db9-3380-472d-8efc-f37f47802ec2"
                }
            ]
        },
        {
            "id": "35e09749-2bdf-4153-b8fc-add3aa20ccd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "158ab91d-20e1-49ef-bdd9-d7ac5bdb9368",
            "compositeImage": {
                "id": "a37035d8-0fd8-46aa-803f-116871f41fc5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35e09749-2bdf-4153-b8fc-add3aa20ccd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f8348cf-04ff-43b2-a7d3-a8ffd08edc3d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35e09749-2bdf-4153-b8fc-add3aa20ccd9",
                    "LayerId": "56559db9-3380-472d-8efc-f37f47802ec2"
                }
            ]
        },
        {
            "id": "c4ef089d-4a96-4126-956d-60ec499c582f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "158ab91d-20e1-49ef-bdd9-d7ac5bdb9368",
            "compositeImage": {
                "id": "255e81a7-b99b-4121-89e4-67fcf7d911cf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4ef089d-4a96-4126-956d-60ec499c582f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d23427a5-dad2-4021-9365-f96c0f30d9f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4ef089d-4a96-4126-956d-60ec499c582f",
                    "LayerId": "56559db9-3380-472d-8efc-f37f47802ec2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "56559db9-3380-472d-8efc-f37f47802ec2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "158ab91d-20e1-49ef-bdd9-d7ac5bdb9368",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}