{
    "id": "41b0a89d-a99c-47fe-892e-6b8a4181f9fe",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fn_start",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "41528fb7-fad8-405d-bb5b-001c7d6c25ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 13,
                "y": 167
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "af92742e-5909-470f-a1e0-97ba0311dfb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 141,
                "y": 167
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "1fe5a691-9b1f-4284-ad8e-dea22ae99422",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 31,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 33,
                "y": 167
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6527c2c8-ca31-4777-b878-2a9cac734458",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 72,
                "y": 68
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "ffc44973-cbac-49ec-909d-4ffa067ccef7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 18,
                "y": 101
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "301d8b03-9c26-484b-a253-7a15a65b5560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 31,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 58,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "148fd27b-d7da-43ae-ba1c-88f32eea45fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 139,
                "y": 35
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "836c9972-947d-43ce-90d6-ed89e1bd88af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 31,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 165,
                "y": 167
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "35a4a282-91e7-4e6c-8d2f-c9429b0b98a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 73,
                "y": 167
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "e693b5a5-7a2f-4955-b4fc-5abe2cb29a6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 63,
                "y": 167
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "23d18cae-a554-4020-ab02-0bea3bb01fef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 31,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 180,
                "y": 134
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "59dc4401-9d8d-491b-b22d-87ddab9394c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 130,
                "y": 101
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "763e0ca4-3ebc-44a3-b47c-f0f4735aaaf8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 123,
                "y": 167
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "ed5691d5-49aa-4c1e-b8b1-f80195977b98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 214,
                "y": 134
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2a4c629d-3490-4486-a842-1f2dd9cebc94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 135,
                "y": 167
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "fbd619a8-37c1-4698-b04a-4f0df7a71868",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 43,
                "y": 167
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "ddb1a244-72b9-4339-a187-177a6536cf82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 134
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "e3ac13a1-d506-4b6e-b9d4-d80e46df8a53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 31,
                "offset": 2,
                "shift": 15,
                "w": 9,
                "x": 203,
                "y": 134
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "6a71807b-5112-4b73-a35b-1edac44dc33a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 114,
                "y": 101
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "3f5b532f-bf44-4bd4-939d-89814a04b7b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 77,
                "y": 134
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "ca001473-37e5-47d2-84c1-48b1b3b552df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 82,
                "y": 101
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5f8700e1-99a7-46ba-bfea-f80b7b089a2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 92,
                "y": 134
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "fcef5db7-cd6f-485c-984b-541b4ba549ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 122,
                "y": 134
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "bab402c9-dfa7-4a1a-851f-087e4de66d66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 137,
                "y": 134
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "00f79d47-b261-42ed-a681-524fbc47281e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 107,
                "y": 134
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "3d3ce95b-f243-4cb4-baf2-8232bd50fc16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 62,
                "y": 134
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "79a635a1-3c84-48a7-a3a6-5ca24bda224a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 153,
                "y": 167
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "72f1815d-a08b-4c54-ae01-2318fd378590",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 129,
                "y": 167
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "8c889730-e7c4-4b46-9087-6fb68329ff7c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 66,
                "y": 101
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "df33fcf1-5125-4d8c-8b23-b85c46402798",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 50,
                "y": 101
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "75f16c13-dac7-478a-bd33-c6b3b68f2ce1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 34,
                "y": 101
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "4e96e888-bba4-4b3b-912d-e0c1ec7a81cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 146,
                "y": 101
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "df176cc9-1bea-424c-87b1-c22673a7d0e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 31,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 30,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "15216fc7-2993-4dd0-ba8e-f7afd7f4f47f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 31,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3fd9f400-bdc7-460b-943b-7b3d896e2363",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 158,
                "y": 35
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3b71aaf1-329f-48a7-8c4b-5fd103a99991",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 31,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 233,
                "y": 2
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "eede7770-80b9-47f6-beb8-b3ecf7e4b334",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 82,
                "y": 35
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "c2de8ad1-f7aa-47ad-9af6-0dd02d2c132b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 31,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 38,
                "y": 68
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "3dcece0e-51de-4a77-9a43-a8571e0cdf93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 31,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 98,
                "y": 101
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6321e9e3-f33a-4880-9730-db710a0dee90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 31,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "8c8ae2d1-39c6-4b58-9b9e-0737ff914d00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 176,
                "y": 35
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "834f175a-2e91-44d9-8e5b-5295b761944a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 31,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 147,
                "y": 167
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "71918bc6-2f7f-4ef3-b469-11cbc5a919e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 166,
                "y": 134
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "01846b31-fb99-4eed-bb1e-ee75dfbf2990",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 101,
                "y": 35
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "c21dda5e-359a-4787-b9b5-985157e9422a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 122,
                "y": 68
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "d8278c12-81fe-48c5-ba0f-09e1a6c88981",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 31,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "36e8949e-7b39-426e-a0db-64695aa8d95e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 194,
                "y": 35
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "10ec6c44-d8a7-4acc-8e66-9a0bc6d6f6ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 31,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 191,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "a2b82fbf-18e4-47f9-980b-a4e3da698de9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 31,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 55,
                "y": 68
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "91b49f78-f47c-4477-ba17-0f131be5de9a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 31,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "a37e60d0-03be-4ea7-89b8-47dbebefe1db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 62,
                "y": 35
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "8359fc71-91a3-45c4-abc1-0822fb6503fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 31,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 212,
                "y": 35
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "99686057-958b-4650-a65e-5e4428091f8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 20,
                "y": 68
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "13678437-bad0-4d9b-a59c-130bcdb01f8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 31,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 230,
                "y": 35
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "cebe899e-d1bd-432e-8350-f30ed1ebcbab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 42,
                "y": 35
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "91932bc2-886f-43f2-93d9-5020f8ca2e05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 31,
                "offset": 0,
                "shift": 25,
                "w": 26,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "62b32cf0-e23c-422b-a854-ddf0cc9d8f94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 22,
                "y": 35
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "3929b6fe-0884-4691-ab80-3970bf04897d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 31,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 2,
                "y": 35
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "eb5aa098-4c34-4500-9277-8c95092a62f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 31,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 2,
                "y": 68
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "965b6aa2-0daa-426b-83cc-984cc2b6f15c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 31,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 83,
                "y": 167
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "0de3e646-a0f0-4445-8b70-e10f99298a20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 53,
                "y": 167
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "96a7f91c-89ad-4e7d-bea3-936174b70edb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 6,
                "x": 109,
                "y": 167
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "a4e18bf6-8153-42f1-90ad-2f309d5eab58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 31,
                "offset": 0,
                "shift": 13,
                "w": 12,
                "x": 152,
                "y": 134
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "572fc9a1-dd2e-4257-a99a-bf3bcb2e72ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 31,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 120,
                "y": 35
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "84194b9e-4361-47e5-af73-abd1d6d9052f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 101,
                "y": 167
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "2f090e76-bc0f-42eb-bcab-21b4472677aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 138,
                "y": 68
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e6f4f5a2-c3b4-4269-87b9-6e35e795996b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 161,
                "y": 101
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "25e485aa-e285-4a7b-8c15-d0458be10672",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 31,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 47,
                "y": 134
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4c7e12b4-098a-40a4-9627-e026ff7ee620",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 154,
                "y": 68
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a4553885-bc95-4640-a5b9-dc11450b74ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 202,
                "y": 68
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "36eb7d44-4682-40d5-8460-32cf5adc1a88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 225,
                "y": 134
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "24d7fe57-29b2-43d8-80db-8d8ab4f09a5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 218,
                "y": 68
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "0c205a26-08d4-411e-a37d-805cfe312768",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 176,
                "y": 101
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "35bd2ea7-91b9-4f82-a562-62c047b3da28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 31,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 117,
                "y": 167
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "6da83e6b-52de-4429-b8be-5bdb0d0a3a12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 31,
                "offset": -2,
                "shift": 6,
                "w": 7,
                "x": 92,
                "y": 167
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "9c2dc27a-daf2-40fa-bf7c-4f43dc36f4e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 31,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 17,
                "y": 134
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "23a518d8-5fe1-4b17-a6ec-1fdb66b9805f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 31,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 159,
                "y": 167
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "284e3074-29f5-44df-a8bd-c53873842886",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 31,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 148,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "6de6054d-7fa9-4fc3-a19e-d5e645b606da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 206,
                "y": 101
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "0c1ad1c1-fcc6-4a9e-8ff6-75816469d8d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 89,
                "y": 68
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "07524bd8-45c2-4c58-8d92-b9ffe0e6e125",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 191,
                "y": 101
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "d85d86de-1131-439e-a11c-bc4a9d5785e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 31,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 106,
                "y": 68
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0ed11555-c575-401d-bac0-8ea7f71319cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 31,
                "offset": 1,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 167
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "0fbbbb30-b1ab-40a8-a43a-d4a8161ab816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 221,
                "y": 101
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "3053d6e3-94f6-4d2b-99f9-fbc16c42b9d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 31,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 23,
                "y": 167
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "ec396c62-2a93-4476-8660-59bf50895922",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 31,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 236,
                "y": 101
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "6ed69588-9521-4eac-b425-b1b5fee689c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 234,
                "y": 68
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "7e775736-75d2-4e76-a31e-fd937fe6844e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 31,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "1bfbdc3f-da49-47fa-9a07-2c18fb792d8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 170,
                "y": 68
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e8951cb1-7413-4968-aa9e-0db8925f1e93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 186,
                "y": 68
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "36203f59-3643-4211-8695-301118123153",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 31,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 2,
                "y": 134
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "00ac9b55-83f8-4575-a429-2a10737547c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 192,
                "y": 134
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "f4d318e7-8ecd-414c-9d02-ad575cd0e1cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 31,
                "offset": 2,
                "shift": 7,
                "w": 3,
                "x": 170,
                "y": 167
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "e9275b45-0672-4755-a94e-982fe693b116",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 31,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 236,
                "y": 134
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "8e82167c-c95a-4c7b-973b-f127f8fde73b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 31,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 2,
                "y": 101
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "c307d5d9-cb7b-4ab3-8cd8-03cb19a10732",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "581a9887-70dc-4544-8353-afaebda75e93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "59caa6c0-2103-48a2-b7a5-5c0ac79b6a90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "4cf12818-8732-4162-94af-b5a611369bab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "912a456d-e105-4693-a7b8-520622eb9d6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "846e108b-9a86-4d2d-9a52-1e98c694caf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 49
        },
        {
            "id": "7b7536c2-f355-470e-9c73-3c3d98bb7f4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "155a791c-a7cc-43e7-b82d-0e731ca3ac78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 84
        },
        {
            "id": "5c2ac202-aa2c-439d-908a-83598a6fbeb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 86
        },
        {
            "id": "39f42ee8-05f9-450e-acdd-00770261855a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "69c36c71-52aa-4bf5-81d0-aac2ecc184bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "411fd46a-0d91-4adb-8b6b-0ca8da5c5ff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "2a536e9c-5bff-413e-9f8e-292686fbf07d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "233f3fbc-fabf-45a0-80a6-d39b5327959a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "5ba4d5e2-8aee-4667-9b4b-fc5c035fa4ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "579c44d2-e3e0-4679-921e-76da3dca6a68",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "e6aa3617-8a21-438e-9278-b4cac2bba853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "4d395d8d-ae94-4b07-a3fc-2487da608b90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "b82940a9-ee2e-42cd-869b-83bdaef55e14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "206b3964-6cfd-45fb-b2dc-c3161e984498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "74e21be8-5833-4a6a-adc8-8c720e9e8294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 89
        },
        {
            "id": "35d0389d-b994-4ef5-885e-bb4486527784",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "379d2b7b-8ab3-4171-9cd0-00489b551fcf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "fba10b1e-3ee5-4684-bfd8-50e59e9d1263",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "aec875cc-4fae-46dc-981e-e6fc3deebc7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 44
        },
        {
            "id": "47c6446f-1242-47d4-93bd-9c1caab21161",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 46
        },
        {
            "id": "80cd7991-879e-46f4-9a1d-cf6065f4e168",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 65
        },
        {
            "id": "04246a14-7eb4-47d6-ae7b-87664e5af1bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "2f0953f5-0e35-4670-92e7-ac607f1636ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "c496c29e-334d-4060-844c-a4146acf2c92",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "60e2bfd3-6295-4fb9-ab7e-215e266be475",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 58
        },
        {
            "id": "4f74392a-768b-460d-b670-ad7bc0a864e7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 59
        },
        {
            "id": "95f4de1a-f734-4135-812f-43a5539c0edd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 65
        },
        {
            "id": "02e8f243-30b4-46c5-a98f-35bccbd83abf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "0931ca5b-87ca-422f-b6e1-9b9527d552b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "d45e6e7f-f719-4740-8a89-2011582284a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "dc3a86cc-5d89-481c-ac9d-c45684ecbfad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "699f4b4d-1f9c-4c61-8069-b33bd01908b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "94da9063-086b-4417-90c5-0ab4faf548df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "3ada1819-4bbf-4063-bf79-97cd083d2273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 115
        },
        {
            "id": "e5af0430-f323-4556-a105-3803b62b4585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "76151806-c555-4e66-87eb-f9596179648f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "1ac242b2-ba80-4e0f-9480-162bb0f4691e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "328552a9-820c-41fe-a2be-864392885e2b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "919d0f7f-6279-4b06-b02b-5b45cff68d60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 894
        },
        {
            "id": "05fd5e99-be31-4d53-a70d-91fe20d31231",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 44
        },
        {
            "id": "bd5b6e7f-6b94-466a-a6a5-7789e627eb42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "0234d725-06b1-4db2-82a6-0f52814b8314",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 46
        },
        {
            "id": "a2cb561b-e899-4715-a72e-43bf2b418d11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "9406b649-0c05-4611-8de6-f445c211d1a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "8b77211f-1084-4d43-a64d-ff8e42aabfc6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 65
        },
        {
            "id": "838bec20-cc90-47fb-9613-ee85b671d4eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "eca9a04c-d95e-4440-970b-6639552ca16a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "330dd51c-a76e-4648-8e0b-bf322f88034a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "023e557f-1fdb-4db5-8300-e49b50782770",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "69a9470a-9547-4167-a206-fa0e7ee8c4dd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "a798eaf3-fd96-4232-ae00-d30f483c9b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 121
        },
        {
            "id": "9d81890f-320b-4034-8c3a-24b771de5296",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "f197d4b8-be57-4b43-a122-704674b80fa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 894
        },
        {
            "id": "5b07f0cb-6c54-4df8-87af-8c0009976120",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "1bdd283b-edf3-4d85-b732-731b9de11bde",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "d9654481-b54b-490f-ba89-f9d60991b74d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "09448e00-8cf9-45c0-80e8-2eaa5023c429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 97
        },
        {
            "id": "565072a6-e203-4dce-abbe-442dfb419e3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 44
        },
        {
            "id": "c3fda302-e8b3-468a-9fdc-afc08eefcb6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "26583af2-378c-4820-a8fc-baa19a0b48fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 46
        },
        {
            "id": "dccb44b4-51e7-4d44-921b-91f0c9631b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "260232dc-7602-43e5-b50b-3fab60cfec4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 59
        },
        {
            "id": "d58f00ae-030e-4296-9a50-50d0c52b8106",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 65
        },
        {
            "id": "49dd7e77-4dd5-4d1c-938b-5e848de33f7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "fa79a0a4-2d5d-4397-a508-f6f2dbb0bd6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "bc1eef3d-e9b4-4355-b42d-29583949f611",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 105
        },
        {
            "id": "e6684844-da2d-496f-b1e5-174955243cab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "8556976e-59d5-4e58-81de-9b935309f8d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "411a877f-d01c-4e42-aa0c-736550e22ccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "0550061d-deb7-4e52-8bb9-62fbddf88e05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "598361fb-f3b5-42fc-89fe-8b99adae352b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "f477037b-6b44-4ca5-a9bc-2c0428b9a10c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "5ca219c4-694e-4376-9fd3-9579dc385057",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 894
        },
        {
            "id": "e1486a4d-b690-4c78-b397-cbca8d22b2be",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "e73186ca-e76b-4c1e-9504-7df69cc08fac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "70e76a4c-7683-4050-89c2-800543a48141",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 114,
            "second": 8217
        },
        {
            "id": "df133bf8-fedd-4f25-9f99-8056f495770a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "8e63630e-ce8b-44e7-a784-ca3011e9eb9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "e7c09f63-7d7e-4d35-b400-65a8dbbb0531",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "3c3e97f8-b3f1-43b3-a543-a94a0af927bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "2ce46e6a-c555-463b-820b-8ea148d7a078",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "6fd1887c-6c66-4e74-a7b1-e615938a50db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 20,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}