{
    "id": "358bac81-3583-42f2-843d-1af041980939",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "fn_title",
    "AntiAlias": 1,
    "TTFName": "",
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "004a67c1-c6d6-4601-ba64-80df84c26ef6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 49,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 31,
                "y": 206
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d4c7a649-41ce-4a67-9e02-e572074c3952",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 49,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 135,
                "y": 206
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b31db8cb-aa02-4cb5-93f2-4d0ac04b541d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 49,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 350,
                "y": 155
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "9ef24bcf-96f3-42cd-8031-3ce79d16e79d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 49,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 418,
                "y": 53
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "13d057c5-371d-4e0f-b811-7fe595053080",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 327,
                "y": 155
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "2d15a1a0-e819-4372-836c-98a7d1fb7830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 49,
                "offset": 1,
                "shift": 38,
                "w": 36,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ac5da150-ef3d-4454-a935-849d77c1565a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 49,
                "offset": 1,
                "shift": 31,
                "w": 30,
                "x": 298,
                "y": 2
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d96332a5-83bb-4707-9963-c530a278d5ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 49,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 125,
                "y": 206
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "f7876374-bca9-42f3-b7e2-501259fb1e5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 49,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 86,
                "y": 206
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "3dffc6df-9853-49a6-aaa4-71905aef46d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 49,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 17,
                "y": 206
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "65bc3be6-2045-42b4-8830-e2bc78642086",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 49,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 387,
                "y": 155
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "c6f68bf8-617b-479c-90fb-eaa86e7ced96",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 49,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 253,
                "y": 104
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "1ddb6c99-eb08-4438-8ae1-5f95f4aa496c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 49,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 155,
                "y": 206
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "74f3197e-b058-4dad-932f-2f69641ded45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 49,
                "offset": 2,
                "shift": 14,
                "w": 12,
                "x": 45,
                "y": 206
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "d0dcb524-0bd5-4eed-bda9-5867d6fbe627",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 49,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 206,
                "y": 206
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "6c05815b-8de1-4f61-b25e-dceef51d1c9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 49,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 490,
                "y": 155
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "dda33989-ae8e-4410-9b8b-3712d9d5daf6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 166,
                "y": 155
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "2506ce8e-6b81-4b5f-9bdb-b3965f11d47c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 49,
                "offset": 3,
                "shift": 24,
                "w": 14,
                "x": 458,
                "y": 155
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "f7e7aa9c-b2d8-4c34-9768-481fb712c15e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 212,
                "y": 155
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "334ed2a7-ad39-4639-85c7-41dba9fdf975",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 26,
                "y": 155
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "cdb68941-eeca-46fa-a5f2-ce3268ce8cd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 49,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 203,
                "y": 104
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "0afb23f5-081a-4256-aa6e-acd44e23b362",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 470,
                "y": 104
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "df30d5e8-3419-466f-88d3-a8582e4c9416",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 446,
                "y": 104
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "0a6cb3a3-5385-4573-b6b0-4ad1c6589b9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 235,
                "y": 155
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "1583e611-61a3-4fb1-a400-cb2300db707d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 258,
                "y": 155
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "12b64098-cf04-4f7b-b2f8-bc57420c986a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 281,
                "y": 155
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a62d5c54-2477-46dc-ab73-985246b4a516",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 49,
                "offset": 4,
                "shift": 14,
                "w": 7,
                "x": 164,
                "y": 206
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "57a4ced9-5d77-4c6a-9bc2-ceca061bfaaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 49,
                "offset": 3,
                "shift": 14,
                "w": 7,
                "x": 173,
                "y": 206
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "f7d10931-be49-4943-8926-cb5ad22eb61a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 49,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 326,
                "y": 104
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e9f9e0fe-cf13-4291-a729-710fc811ac89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 49,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 228,
                "y": 104
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f3f528af-685f-47d9-b83c-cb41fabf79ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 49,
                "offset": 2,
                "shift": 25,
                "w": 22,
                "x": 2,
                "y": 155
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "823681cc-62bc-4953-a52d-8bea0d4e4854",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 49,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 178,
                "y": 104
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "0af2846d-a777-4cbf-b3c7-f819c68e923d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 49,
                "offset": 1,
                "shift": 42,
                "w": 41,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "2ac6e95c-6571-41c7-b6a4-7b144d1626d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 49,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 265,
                "y": 2
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "bd7f5e7a-f7d5-40f3-ba07-93f0525919db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 49,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 203,
                "y": 53
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "4c242fdb-5a08-48ba-9763-93237ada24c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 49,
                "offset": 2,
                "shift": 31,
                "w": 27,
                "x": 62,
                "y": 53
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d14b8a08-638b-4529-8ad0-adbbdaed5d0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 49,
                "offset": 3,
                "shift": 31,
                "w": 26,
                "x": 175,
                "y": 53
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "68a0715a-a5bc-4a30-a0fd-2f43ebd68cda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 49,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 366,
                "y": 53
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "73762b75-b297-4051-8daf-93997ed2c77c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 49,
                "offset": 3,
                "shift": 26,
                "w": 22,
                "x": 398,
                "y": 104
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4991f7d7-c8d9-4627-853f-dcf2d6f1ae17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 49,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 426,
                "y": 2
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "6bc2c301-c992-4011-bd00-db9a709bc4c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 49,
                "offset": 3,
                "shift": 31,
                "w": 25,
                "x": 259,
                "y": 53
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "fc0f75d8-8049-4cdb-9165-cf37bca1b887",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 49,
                "offset": 2,
                "shift": 12,
                "w": 8,
                "x": 145,
                "y": 206
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "d0795174-730d-4b65-acc0-1f473808c9a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 49,
                "offset": 0,
                "shift": 24,
                "w": 21,
                "x": 143,
                "y": 155
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "db768a8d-ff93-4152-8039-e85a37f9f92a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 49,
                "offset": 3,
                "shift": 31,
                "w": 28,
                "x": 2,
                "y": 53
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "d80d57b0-4d61-470d-88f0-6403f2c6f192",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 49,
                "offset": 3,
                "shift": 26,
                "w": 22,
                "x": 302,
                "y": 104
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "278b08b8-6312-46c9-874e-bb4cc1f2826d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 49,
                "offset": 3,
                "shift": 36,
                "w": 30,
                "x": 330,
                "y": 2
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "f06699c2-7d63-4d3f-9332-f414ed05a0f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 49,
                "offset": 3,
                "shift": 31,
                "w": 25,
                "x": 313,
                "y": 53
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "93dca0db-ebf2-4db0-a0e6-bf3fb0534659",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 49,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "31eaf432-c659-4635-87d3-60b2fea81b0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 49,
                "offset": 3,
                "shift": 29,
                "w": 24,
                "x": 470,
                "y": 53
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "336cc4a2-3ada-424e-8d89-df7a24e5eb6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 49,
                "offset": 1,
                "shift": 33,
                "w": 32,
                "x": 198,
                "y": 2
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "f05b4e20-30fe-4f38-b390-8d16af44b393",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 49,
                "offset": 3,
                "shift": 31,
                "w": 28,
                "x": 32,
                "y": 53
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "0528830a-fa25-4364-84d5-0832ed582b4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 49,
                "offset": 1,
                "shift": 29,
                "w": 26,
                "x": 91,
                "y": 53
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "65ffe607-278a-4919-aa2e-57b5b1b68265",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 49,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 231,
                "y": 53
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "1f71ae38-5568-47f4-851c-1e92d7a1e930",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 49,
                "offset": 3,
                "shift": 31,
                "w": 25,
                "x": 286,
                "y": 53
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "74b70c6e-0ef1-4351-ae23-affec9fff42d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 49,
                "offset": -1,
                "shift": 29,
                "w": 30,
                "x": 362,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "b1405f4e-5bb8-4ee2-8d1a-ad4c567394c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 49,
                "offset": 0,
                "shift": 41,
                "w": 41,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "5d771620-89db-47ca-b6e3-7afab0d74dfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 49,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 457,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "74aa54c4-cc73-43cf-8b20-617874c032c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 49,
                "offset": -1,
                "shift": 29,
                "w": 30,
                "x": 394,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "fffaa33b-4302-4578-a61c-3cc9fc2cf01c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 49,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 147,
                "y": 53
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "c0732cad-1553-4cca-9959-b5fb38631284",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 49,
                "offset": 3,
                "shift": 14,
                "w": 11,
                "x": 112,
                "y": 206
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "7160837e-399a-418d-8025-8a16b2baa08a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 49,
                "offset": -1,
                "shift": 12,
                "w": 13,
                "x": 2,
                "y": 206
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "7c3e13f8-1d3b-45a6-b640-b91ad388fe1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 49,
                "offset": 0,
                "shift": 14,
                "w": 12,
                "x": 59,
                "y": 206
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "3be00729-aa40-4dd9-8d86-9987c621d39c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 49,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 120,
                "y": 155
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b967aa4c-c73c-4f17-b8ae-9ec9b9f6292e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 49,
                "offset": -1,
                "shift": 24,
                "w": 26,
                "x": 119,
                "y": 53
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "393f19bc-86c4-4caa-a3fe-29e75eda96be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 49,
                "offset": 0,
                "shift": 14,
                "w": 11,
                "x": 99,
                "y": 206
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "6d6d38e1-00f6-40bf-ae06-c15b63389c55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 422,
                "y": 104
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "f5619a7a-cc38-4dd4-8833-32f799f80df8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 49,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 128,
                "y": 104
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d73be18b-7b4e-43f0-8e4f-90195f610fa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 374,
                "y": 104
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "8fdf07f8-075f-401b-948f-4d2463c7c913",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 49,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 103,
                "y": 104
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "be038a42-a2bd-456d-9c26-0d26c43c4730",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 350,
                "y": 104
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "3f61906e-d23b-472a-a8e7-323c8c44ccb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 49,
                "offset": 0,
                "shift": 14,
                "w": 16,
                "x": 369,
                "y": 155
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "52a3ad4d-03c9-49e4-a36e-dc6b5f2b28ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 49,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 78,
                "y": 104
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a953a38d-fede-454e-bc2c-6f18c0f7fdce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 49,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 97,
                "y": 155
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "37de9276-af72-4b64-a1eb-ba5f1b45d56c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 49,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 182,
                "y": 206
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "dae98361-ecd4-48cb-84e6-be1a539ac153",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 49,
                "offset": -2,
                "shift": 12,
                "w": 11,
                "x": 73,
                "y": 206
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "64ca3669-191b-44ce-80b6-9f2501118ed7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 49,
                "offset": 2,
                "shift": 24,
                "w": 22,
                "x": 278,
                "y": 104
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "6c4e64e3-b679-4cb6-b310-be2d510fcaad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 49,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 198,
                "y": 206
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "dd0fac06-60e4-4fbb-8b0b-2c1bb68815bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 49,
                "offset": 2,
                "shift": 38,
                "w": 34,
                "x": 162,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "055ecda3-a4a3-413b-97e4-55a3c7feaf56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 49,
                "offset": 3,
                "shift": 26,
                "w": 21,
                "x": 189,
                "y": 155
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "590bbea5-8d64-4e52-9783-dd0df871a56c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 49,
                "offset": 1,
                "shift": 26,
                "w": 24,
                "x": 444,
                "y": 53
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a4f1aee2-83f4-4950-87db-aae909e4691c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 49,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 53,
                "y": 104
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "02a47770-e998-4209-84fb-79a1e22f3f1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 49,
                "offset": 1,
                "shift": 26,
                "w": 23,
                "x": 28,
                "y": 104
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "5b113d7c-5f75-4218-82a5-eb7068512dbf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 49,
                "offset": 2,
                "shift": 17,
                "w": 16,
                "x": 423,
                "y": 155
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "832becdd-f5ec-41ce-99aa-47bebffa864c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 49,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 74,
                "y": 155
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "85cfad5b-cfb5-471e-92fe-f958587d96ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 49,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 474,
                "y": 155
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6689f2e2-3b45-4f59-b29a-6e15735e7a80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 49,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 50,
                "y": 155
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "9e6ada23-7178-4b3b-866f-c57748646d5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 49,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 340,
                "y": 53
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "d41e8315-90d1-4a33-906d-8da49465cd49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 49,
                "offset": 0,
                "shift": 33,
                "w": 34,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "e950a041-f982-44b9-bf1b-8e334045be9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 49,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 2,
                "y": 104
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "11331879-0613-4319-ae3b-5032fe4ae589",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 49,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 392,
                "y": 53
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "7576fc6b-fb65-45f3-95ad-fe51605b7b40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 49,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 304,
                "y": 155
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e8957ee6-a8a9-4b1b-9a51-d6e0eba81a81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 49,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 441,
                "y": 155
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "7f2beaa4-f9f5-4a32-9266-03f8c6ceea77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 49,
                "offset": 3,
                "shift": 12,
                "w": 6,
                "x": 190,
                "y": 206
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "5b2ab894-17a7-46dd-a1ea-c98e547fdbd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 49,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 405,
                "y": 155
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "465ad8c4-9adf-4add-9797-71dd63d753f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 49,
                "offset": 1,
                "shift": 25,
                "w": 23,
                "x": 153,
                "y": 104
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "8f012c6d-23b5-40b7-96dc-9551452aad9c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 65
        },
        {
            "id": "e8e9a80e-775d-4b02-952d-b2af5ee060bb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "a62971da-352c-4a4c-9caf-d09c0a7bf909",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 902
        },
        {
            "id": "ee406c76-4d1f-4273-8b61-32af78a6556b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 913
        },
        {
            "id": "04036ad0-6da5-4d00-be7b-ae315eb66604",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 916
        },
        {
            "id": "10e4e3fa-f3de-4cc8-9722-c2d2b3645ce0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 32,
            "second": 923
        },
        {
            "id": "56989bd4-5c52-4498-b015-e77fec8a7e71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "8643c55d-0b35-449a-9fe3-29abe7e9d82b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "4914f47c-d5f5-45d9-8740-d3d49924aceb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 49,
            "second": 49
        },
        {
            "id": "a92d13dc-bbef-47a5-b4f8-4bc4d968a739",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 32
        },
        {
            "id": "df0b56c6-2d34-4492-a363-5bfbfec0fffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 84
        },
        {
            "id": "8bbb741c-049e-4f59-91e3-699e8234325b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 86
        },
        {
            "id": "d48a5c17-1ab0-4bd1-adfc-5624ca93fd64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "9ff90ad0-0f6c-4e7a-b1ec-2179f328afbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 89
        },
        {
            "id": "2c419b82-76f7-4e94-b3cc-5950f98d02ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 118
        },
        {
            "id": "57e6e2cc-9fce-438d-99da-c5edea03bc6d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "2fedecdc-9a12-4059-adf9-cdf21e89d799",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 121
        },
        {
            "id": "e5ae9d7d-a686-4a0c-8184-c2fb42c2156c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 160
        },
        {
            "id": "fbdaa49e-1022-4034-b7c2-aea923f79255",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 8217
        },
        {
            "id": "39d44a16-82e4-429c-9032-fb04f1c30e78",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 44
        },
        {
            "id": "e9c198e1-189c-4524-b0d6-deac505dc055",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 46
        },
        {
            "id": "0745c5b4-c27b-498c-a6ed-1d0a3aa71a54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 65
        },
        {
            "id": "ff26886d-0211-48f7-805c-1efbe542f696",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 32
        },
        {
            "id": "5c796335-27e5-49e1-9a1a-021c13713bf4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 84
        },
        {
            "id": "f32a0e61-276d-4503-8dd3-0cc6567656fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 86
        },
        {
            "id": "a4b16c31-d981-4c56-ab0d-166dc886c0fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "38493afa-10c5-4a46-9f76-544b0e2938f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 89
        },
        {
            "id": "0d593f31-a7c4-41fb-964e-d83ec3ac2a3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "509123bf-c169-416e-8a2a-71126a1bdd18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 160
        },
        {
            "id": "206748e3-911b-4548-9276-0e8476ce8c15",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 8217
        },
        {
            "id": "3fa5c665-a714-46b4-841c-fc451a96dccc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "61d5a05b-b6c6-409f-a4c2-7584793b611d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 44
        },
        {
            "id": "6f8fd833-85b3-4dc8-8dfd-67357c592b6f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 80,
            "second": 46
        },
        {
            "id": "01e9e37d-d6e5-4b77-b585-71d5271cb10d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 65
        },
        {
            "id": "ad4653d3-d244-4a70-b69f-fabcfef2828a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "0b255f3f-9aa7-475a-80f6-9006f0fb957a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "37650817-3c16-4cd7-97f1-6eabc2a12f35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "5b1d3754-7d1e-44f4-97cd-1c2310e9cb4e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 82,
            "second": 89
        },
        {
            "id": "e1ea466f-0690-482f-a53a-64ae34e31ae8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 44
        },
        {
            "id": "784992f9-65a2-4182-8d3e-cc2a422e6fff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 45
        },
        {
            "id": "7513a957-c559-4ce9-83ae-f24cb01d5de0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 46
        },
        {
            "id": "126a822a-7436-4199-99c2-2d2a06418e8b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 58
        },
        {
            "id": "0c95bdbf-534b-463d-8b6b-9465804fbadf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 59
        },
        {
            "id": "c09c6e57-dc91-46a9-b1a7-ac3d4a084805",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 65
        },
        {
            "id": "1fdd6d6b-d105-4e62-820b-0f08a1bfc2a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "2e0b7f7b-eada-472d-a8b7-d4d6abf7b939",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 97
        },
        {
            "id": "d08d6789-5d01-4ef9-b212-01201bca7fe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 99
        },
        {
            "id": "d5facb27-9f39-43f2-b976-e7fd95c93719",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 101
        },
        {
            "id": "2fa2dc17-4d43-44cc-8889-027efb5cbc77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 105
        },
        {
            "id": "19f70501-2781-4a08-b85a-0334b042e846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 111
        },
        {
            "id": "4d0668cc-a0aa-4758-ade8-16220d5628b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "6a8e6104-0073-4483-8cf8-9947d0e76966",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 115
        },
        {
            "id": "09ee6d5c-ce55-4c3c-aaf3-b3b55c73744f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 117
        },
        {
            "id": "0d788f5c-f9fa-4c5d-b703-82f7734c71ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 119
        },
        {
            "id": "5cbd1334-30e2-4cba-9354-46bf96bdd241",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 121
        },
        {
            "id": "8ed03c6a-db34-4af3-9c59-b7081fe03343",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 173
        },
        {
            "id": "e244c1ac-0408-4e0a-9ba7-0338811787d1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 894
        },
        {
            "id": "db5d0c9e-81f0-4ab4-bcff-5f5f70f75193",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 44
        },
        {
            "id": "e07aa9ec-7e07-4028-9220-215a8fab5066",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 45
        },
        {
            "id": "676658ba-167e-48e0-8ecc-9d645cbe07e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 46
        },
        {
            "id": "f153d2d7-b7bd-4471-8f6b-b8b00e36b68e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "21584dd5-e7e8-4ff6-99a9-189c7d34b6a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "a84d1705-9976-4ff6-bec3-6aaca569016e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 65
        },
        {
            "id": "6b323b17-8521-45ed-ac25-aa88966d8e2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "470831ba-dac4-41cd-a6b4-9975e94975a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 101
        },
        {
            "id": "02b31353-7440-4db2-9c76-2b0b281fbc7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "ab98ecd8-d85d-4684-b423-b96c65c005df",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 111
        },
        {
            "id": "2e4feefa-8532-4a9a-8015-02a437b4a45b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 114
        },
        {
            "id": "bb4ebd64-f112-4692-9e59-fd899c420b10",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "3f81bf78-296f-423d-8407-50e9f29a1781",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 121
        },
        {
            "id": "1ca0a379-8fce-4a39-bc74-1d48f3f7133b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 173
        },
        {
            "id": "4fba59f6-f32e-4310-a76c-97c277601854",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 894
        },
        {
            "id": "ac5696ae-03ba-4a55-b0a4-04f41c4ad8b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 44
        },
        {
            "id": "ab85afde-2f80-4122-a9af-2b5b3d5f68d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "c6ccf9cc-ee07-4ae6-939c-cc30ec0a142a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 46
        },
        {
            "id": "c6b724d1-c05e-45dd-8078-6eda2b1d3ec8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "78ae7a44-f41a-4c3d-9bd4-fffd9bbcd718",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "580f6e85-c681-41bd-bf8f-c3434f8d8e43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "b3aba83b-b3ef-403b-aa0f-595dd532af7f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "04599730-5854-4fad-9d6b-9be1249f42e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "0bcf0e91-f75f-4a08-a3ef-142652143f27",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "b6a63641-ff27-43c3-9df7-c6b9efc5be13",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "210b987f-ac17-4e72-9298-12580614d79b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "9f93e7ed-e5fa-43a1-b611-5111d78c8111",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 121
        },
        {
            "id": "c2543d80-5dbd-4f65-83be-fde3a59d41a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "cde6e64a-eb18-439e-b86a-bdfa2d23fad0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "268e1e53-957b-4fd2-b381-f42c6de6a633",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "12380861-362e-43c6-beb9-60832606f8ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 44
        },
        {
            "id": "6cd1d9a3-9c3c-4105-aba5-1f60aedc7032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 45
        },
        {
            "id": "e0ad0433-c08e-4c76-af48-cd0de16f4eb9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 89,
            "second": 46
        },
        {
            "id": "5d9419f4-56f6-460f-a7d4-ca51c0f17ca3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 58
        },
        {
            "id": "dcde5ff9-92ee-4690-b8bc-f80a45e8892d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 59
        },
        {
            "id": "11d00ded-2cc9-4b46-a9b1-c9e8a85a6048",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 65
        },
        {
            "id": "ccdaa6c4-408f-4f18-a5e2-5c26f4e302e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "d59cb935-d2d9-45f5-bb9d-78c458acd3d2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "cc5e3792-b49e-4564-9adf-1fe6c9fefb88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 105
        },
        {
            "id": "d53825f1-dd8a-44b5-8a65-30d71bc4d090",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 111
        },
        {
            "id": "8b306ac3-eeff-4242-b729-824e92cc01e6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "f047a2f5-b26b-4e26-8b02-a902a439bcbf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 113
        },
        {
            "id": "8a05420e-1af5-4493-bd7c-e1b0bd9315da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 117
        },
        {
            "id": "8910bac7-1a66-4ed8-87e4-c30a0b270f95",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "405fc2cb-f31f-45ee-95fb-a8dfa4363ed8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "838a619b-3c40-42d5-9074-a87bbed75e37",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 173
        },
        {
            "id": "975fd619-075e-4f98-aba8-74578d9446aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 894
        },
        {
            "id": "55fa900c-bc64-4c33-be27-bfe0b8ea05fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "c246929d-0d77-4cee-99f9-0b7e474fd4b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "42daa9dd-05f0-4dd8-9d3d-5e293d249b6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "938ebb2e-01f9-447b-8d12-f392bf4c2957",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "d229a9e4-7b6e-46b4-b47f-b3f8ac4e4e6e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 44
        },
        {
            "id": "cef55232-8ff2-4a15-843c-3458c3740907",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 118,
            "second": 46
        },
        {
            "id": "a575ae32-f0f0-472d-9d5a-1f8338def0b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "ee89624d-b7fd-432d-b714-42d3f4a1a72b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "f13a8f7f-af02-4cf9-a4f3-8036904caa0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 44
        },
        {
            "id": "7f1ca168-dd21-4376-9c1d-19ddac145910",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG",
    "size": 32,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}