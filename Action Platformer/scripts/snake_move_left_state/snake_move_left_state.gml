///snake_move_left_state()

// Check for wall or ledge
var wall_at_left = place_meeting(x - 1, y, obj_Solid);
var ledge_at_left = !position_meeting(bbox_left - 1, bbox_bottom + 1, obj_Solid);

if (wall_at_left || ledge_at_left) {
	state = snake_move_right_state;
} else {
	// Sprite control
	image_xscale = -1;

	// Move
	x -= spd;
}