///exit_state()

if (image_alpha > 0) {
	sprite_index = spr_player_exit;
	image_alpha -= 0.25;
} else {
	if (room != room_last) {
		room_goto_next();	
	} else {
		// Score the game
		score = obj_Player_Stats.sapphires * 100;
		
		ini_open("settings.ini");
		obj_Player_Stats.high_score = ini_read_real("Score", "High Score", 0);
		
		if (score > obj_Player_Stats.high_score){
			obj_Player_Stats.high_score	= score;
			ini_write_real("Score", "High Score", obj_Player_Stats.high_score);
		}
		ini_close();
		
		room_goto(rm_high_score);
	}
}