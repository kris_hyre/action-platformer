///boss_chase_state()

var distance_to_player = point_distance(x, y, obj_Player.x, y); // horizontal distance to player

// Check to see if over player or close to wall
if (distance_to_player < (sprite_width / 2 - 16)) or (place_meeting(x - 1, y, obj_Solid)) or (place_meeting(x + 1, y, obj_Solid)){ 
	state = boss_smash_state; // Smash!
	audio_play_sound(snd_jump, 6, false);
	hspd = 0;
} else {
	hspd = (obj_Player.x - x) * 0.05; // approach the player	
}

move(obj_Solid);
