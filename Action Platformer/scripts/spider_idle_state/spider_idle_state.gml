///spider_idle_state()
if (instance_exists(obj_Player)) {
	var dis = distance_to_object(obj_Player);
	
	if ((dis < sight) && (alarm[0] <= 0)) {
		image_speed = 0.5;
		
		if (obj_Player.x != x) {
			image_xscale = sign(obj_Player.x-x); // faces the sprite at the player
		}
	}
}