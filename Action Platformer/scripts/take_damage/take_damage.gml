///take_damage()

if (state != hurt_state) {		// Get hurt
		
	// Ouch!
	audio_emitter_pitch(audio_em, 1.6);
	audio_emitter_gain(audio_em, 1.4);
	audio_play_sound_on(audio_em, snd_ouch, false, 8);
		
	// Knockback and hurt state
	image_blend = make_color_rgb(220, 150, 150);
	vspd = -12;
	hspd = sign(x -other.x ) * 8;
	state = hurt_state;
	move(obj_Solid);
		
	// Remove health
	if (instance_exists(obj_Player_Stats)) {
		obj_Player_Stats.hp -= 1;
	}
}
	