///hurt_state()
sprite_index = spr_player_damage;

if (hspd != 0) {
	image_xscale = sign(hspd);
}

// Apply grav

if (!place_meeting(x, y + 1, obj_Solid)) {
	vspd += grav;
} else {
	vspd = 0; 
	apply_friction(acc);
}

direction_move_bounce(obj_Solid);

// Change back to move state

if ((hspd == 0) && (vspd == 0)) {
	image_blend = c_white;
	
	if (obj_Player_Stats.hp <= 0){
		room_restart();
		obj_Player_Stats.sapphires = 0;
		obj_Player_Stats.hp = obj_Player_Stats.max_hp;
		
		if (audio_is_playing(mus_Clenched_Teeth)){
			audio_stop_sound(mus_Clenched_Teeth);
		}
	}
	
	state = move_state;
}
