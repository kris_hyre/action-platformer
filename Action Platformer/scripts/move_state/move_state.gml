///move_state()


if (!place_meeting(x, y + 1, obj_Solid)) {
	vspd += grav;
	
	sprite_index = spr_player_jump;
	image_speed = 0;
	image_index = vspd > 0; // Set index to 0 or 1
	
	if (up_released && vspd < -6) {
		vspd = -6;
	} 
} else {
	vspd = 0;
	// Jump
	if (up && !place_meeting(x,y,obj_Exit)) {
		vspd = -jspd;
		audio_play_sound(snd_jump, 5, false);
	}
	
	if (hspd == 0) {
		sprite_index = spr_player_idle;
	} else {
		sprite_index = spr_player_walk ;
		image_speed = 0.75;
	}
}

// Left or Right move
if (right || left) {
	hspd += (right - left) * acc;
	if (hspd > spd) {
		hspd = spd;
	}
	
	if (hspd < -spd) {
		hspd = -spd;
	}
} else {
	apply_friction(acc);
}

// Sprite Flip
if (hspd != 0) {
	image_xscale = sign(hspd);
}

// Landing audio
if (place_meeting(x, y + vspd + 1, obj_Solid) and vspd > 0){
	audio_emitter_pitch(audio_em, random_range(0.8, 0.12));
	audio_emitter_gain(audio_em, 0.2);
	audio_play_sound_on(audio_em, snd_step, false, 6);
}

move(obj_Solid);

/// Check for a ledge

var falling = y - yprevious > 0;
var wasnt_wall = !position_meeting(x + 17 * image_xscale, yprevious, obj_Solid);
var is_wall = position_meeting(x + 17 * image_xscale, y, obj_Solid);

if (falling && wasnt_wall && is_wall) {
	hspd = 0;
	vspd = 0;
	
	while (!place_meeting(x + image_xscale, y, obj_Solid)) {
		x += image_xscale;
	}
	
	while (position_meeting(x + 17 * image_xscale, y - 1, obj_Solid)) {
		y -= 1;
	}
	
	sprite_index = spr_player_ledge_grab;
	state = ledge_grab_state;
	
	// grab audio
	audio_emitter_pitch(audio_em, 1.5);
	audio_emitter_gain(audio_em, 0.1);
	audio_play_sound_on(audio_em, snd_step, false, 6);
} 