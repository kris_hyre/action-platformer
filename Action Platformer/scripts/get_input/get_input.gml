// Get input from p;ayer

right = keyboard_check(vk_right) || keyboard_check(ord("D"));
left = keyboard_check(vk_left) || keyboard_check(ord("A"));
up = keyboard_check_pressed(vk_up) || keyboard_check_pressed(ord("W")); 
up_released = keyboard_check_released(vk_up) || keyboard_check_released(ord("W"));
down = keyboard_check(vk_down) || keyboard_check(ord("S")); 

// Gamepad override
var gp_id = 0;
var threshold = 0.5;

if (gamepad_is_connected(gp_id)){
	right = (gamepad_axis_value(gp_id, gp_axislh) > threshold || keyboard_check(vk_right) || keyboard_check(ord("D")));
	left = (gamepad_axis_value(gp_id, gp_axislh) < -threshold || keyboard_check(vk_left) || keyboard_check(ord("A")));
	up = (gamepad_button_check_pressed(gp_id, gp_face1) || keyboard_check_pressed(vk_up) || keyboard_check_pressed(ord("W")));
	up_released = (gamepad_button_check_released(gp_id, gp_face1) || keyboard_check_released(vk_up) || keyboard_check_released(ord("W")));
	down = (gamepad_axis_value(gp_id, gp_axislv) > threshold || keyboard_check(vk_down) || keyboard_check(ord("S"))); 	
}