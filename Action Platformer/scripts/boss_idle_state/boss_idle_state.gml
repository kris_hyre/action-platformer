///boss_idle_state()

var distance_to_player = point_distance(x, y, obj_Player.x, obj_Player.y);

if (distance_to_player <= 128){
	state = boss_lift_state;
	
	if (!audio_is_playing(mus_Clenched_Teeth)){
		audio_emitter_gain(audio_em, 0.5);
		audio_play_sound_on(audio_em, mus_Clenched_Teeth, true, 10);
	}
}