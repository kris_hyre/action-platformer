///snake_move_right_state()

// Check for wall or ledge
var wall_at_right = place_meeting(x + 1, y, obj_Solid);
var ledge_at_right = !position_meeting(bbox_right + 1, bbox_bottom + 1, obj_Solid);

if (wall_at_right || ledge_at_right) {
	state = snake_move_left_state;
} else {
	// Sprite control
	image_xscale = 1;

	// Move
	x += spd;
}




